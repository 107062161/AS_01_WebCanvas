let canvas = document.getElementById("drawing-board");
let ctx = canvas.getContext('2d');
let screen_Width = document.documentElement.clientWidth;
let screen_Height = document.documentElement.clientHeight;

canvas.width = screen_Width;
canvas.height = screen_Height;
//text
let text_btn = document.getElementById("text");
let isText = false;
var text_size;
let text_font = document.getElementById("text-font");
//triangle
let tri_btn = document.getElementById("triangle");
let isTriangle = false;
//rectangle
let rec_btn = document.getElementById("rectangle");
let isRectangle = false;
//circle
let cir_btn = document.getElementById("circle");
let isCircle = false;
//line
let line_btn = document.getElementById("make-line");
let isLine = false;
//Drag
let isDragging = false;
var snapshot;
var dragStartLocation;
//download
let download = document.getElementById("save");
//upload
let imageLoader = document.getElementById('upload_img');
imageLoader.addEventListener("change", DrawUpload, false);
//redo undo
let redo = document.getElementById('redo');
let undo = document.getElementById('undo');
let memory = [];
let step = -1;
//color
let color = document.getElementById('favcolor');
//Pen size
let Size = document.getElementById('range1');
let pen_size = 5;
//Draw and bruch state 
let isDrawing = false;
let isBruching = true;
let bruch = document.getElementById("bruch");
//Mouse coordinate
let lastPoint = { x: undefined, y: undefined };
//Clear the screen
let clearScreen = document.getElementById("clear");
//Eraser
let isErasing = false;
let eraser = document.getElementById("eraser");

download.onclick = function() {
    let imgUrl = canvas.toDataURL('image/png');
    let down = document.createElement('a');
    document.body.appendChild(down);
    down.href = imgUrl;
    down.download = "New"
    down.click();
}

Size.onchange = function() {
    pen_size = parseInt(range1.value * 2);
}

eraser.onclick = function() {
    document.body.style.cursor = "url('./image/eraser.png'), auto";
    isErasing = true;
    isBruching = false;
    isLine = false;
    isCircle = false;
    isTriangle = false;
    isRectangle = false;
    isText = false;
}

bruch.onclick = function() {
    document.body.style.cursor = "url('./image/pen.png'), auto";
    isErasing = false;
    isBruching = true;
    isLine = false;
    isCircle = false;
    isTriangle = false;
    isRectangle = false;
    isText = false;
}

undo.onclick = function() {
    console.log("Undo");
    DrawUndo();
}

redo.onclick = function() {
    DrawRedo();
}

text_btn.onclick = function() {
    document.body.style.cursor = "url('./image/text.png'), auto";
    isText = true;
    isBruching = false;
    isErasing = false;
    isLine = false;
    isCircle = false;
    isTriangle = false;
    isRectangle = false;
}

line_btn.onclick = function() {
    document.body.style.cursor = "url('./image/pen.png'), auto";
    isLine = true;
    isCircle = false;
    isBruching = false;
    isErasing = false;
    isTriangle = false;
    isRectangle = false;
    isText = false;
}

cir_btn.onclick = function() {
    document.body.style.cursor = "url('./image/circle.png'), auto";
    isCircle = true;
    isLine = false;
    isBruching = false;
    isErasing = false;
    isTriangle = false;
    isRectangle = false;
    isText = false;
}

tri_btn.onclick = function() {
    document.body.style.cursor = "url('./image/triangle.png'), auto";
    isTriangle = true;
    isCircle = false;
    isLine = false;
    isBruching = false;
    isErasing = false;
    isRectangle = false;
    isText = false;
}

rec_btn.onclick = function() {
    document.body.style.cursor = "url('./image/rectangle.png'), auto";
    isRectangle = true;
    isCircle = false;
    isLine = false;
    isBruching = false;
    isErasing = false;
    isTriangle = false;
    isText = false;
}

//clear event----
clearScreen.onclick = function(e) {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        console.log("--reset message--");
        console.log("Draw : " + isDrawing);
        console.log("Bruch : " + isBruching);
        console.log("Eraser : " + isErasing);
        console.log("-----------------");
}
    //-----clear event

//mouse event---
canvas.onmouseup = function() {
    isDrawing = false;
    save();

    if (isDragging) {
        isDragging = false;
        dragStop();
    }
}

function dragStart(event) {
    isDragging = true;
    dragStartLocation = getCanvasCoordinates(event);
    takeSnapshot();
}

function drag(e) {
    var pos;
    if (isDragging === true) {
        restoreSnapshot();
        pos = getCanvasCoordinates(event);
        if (isCircle) draw2(pos);
        else if (isLine) draw1(pos);
        else if (isTriangle) draw3(pos);
        else draw4(pos);
    }
}

function dragStop() {
    restoreSnapshot();
    var pos = getCanvasCoordinates(event);
    if (isCircle) draw2(pos);
    else if (isLine) draw1(pos);
    else if (isTriangle) draw3(pos);
    else draw4(pos);
}

function draw1(pos) { //Line
    ctx.beginPath();
    ctx.strokeStyle = color.value;
    ctx.lineWidth = pen_size;
    ctx.lineCap = 'round';
    ctx.moveTo(dragStartLocation.x, dragStartLocation.y);
    ctx.lineTo(pos.x, pos.y);
    ctx.stroke();
}

function draw2(pos) { //Circle
    var radius = Math.sqrt(Math.pow((dragStartLocation.x - pos.x), 2) + Math.pow((dragStartLocation.y - pos.y), 2));
    ctx.beginPath();
    ctx.strokeStyle = color.value;
    ctx.lineWidth = pen_size;
    ctx.arc(dragStartLocation.x, dragStartLocation.y, radius, 0, 2 * Math.PI, false);
    ctx.stroke();
}

function draw3(pos) { // Triangle
    ctx.beginPath();
    ctx.moveTo(dragStartLocation.x + (pos.x - dragStartLocation.x) / 2, dragStartLocation.y)
    ctx.strokeStyle = color.value;
    //ctx.fillStyle = color.value;
    ctx.lineWidth = pen_size;
    ctx.lineTo(pos.x, pos.y);
    ctx.lineTo(dragStartLocation.x, pos.y);
    ctx.closePath();
    ctx.stroke();
}

function draw4(pos) { //rectangle
    ctx.beginPath();
    ctx.strokeStyle = color.value;
    //ctx.fillStyle = color.value;
    ctx.lineWidth = pen_size;
    ctx.rect(dragStartLocation.x, dragStartLocation.y, pos.x - dragStartLocation.x, pos.y - dragStartLocation.y);
    ctx.closePath();
    ctx.stroke();
}

function takeSnapshot() {
    snapshot = ctx.getImageData(0, 0, canvas.width, canvas.height);
}

function restoreSnapshot() {
    ctx.putImageData(snapshot, 0, 0);
}

function drawText(e, x, y) {
    text_size = pen_size + 20;
    ctx.font = text_size + "pt " + text_font.value;
    console.log(ctx.font);
    ctx.fillText(text.value, x, y);
}

canvas.onmousedown = function(e) {
    isDrawing = true;
    var x = event.clientX - canvas.getBoundingClientRect().left;
    var y = event.clientY - canvas.getBoundingClientRect().top;
    lastPoint = { 'x': x, 'y': y };
    if (isLine || isCircle || isTriangle || isRectangle) dragStart();
    else if (isText) drawText(e, x, y);
    else if (isBruching == true) {
        ctx.fillStyle = color.value;
        ctx.strokeStyle = color.value;
        drawCircle(x, y, pen_size);
    } else if (isErasing == true) {
        ctx.clearRect(x , y , pen_size, pen_size);
    } else {
        lastPoint = { 'x': x, 'y': y };
    }
};

canvas.onmousemove = function(e) {
    if (isDrawing) {
        var x = event.clientX - canvas.getBoundingClientRect().left;
        var y = event.clientY - canvas.getBoundingClientRect().top;
        if (!isDrawing) return;
        if (isErasing) {
            ctx.clearRect(x   , y , pen_size, pen_size);
        }
        if (isLine || isCircle || isTriangle || isRectangle) drag();
        let newPoint = { 'x': x, 'y': y };
        if (isBruching) drawLine(lastPoint.x, lastPoint.y, newPoint.x, newPoint.y);
        lastPoint = newPoint;
    }
};
//-----mouse event

function drawCircle(x, y, R) {
    ctx.beginPath();
    ctx.arc(x, y, pen_size * 2.4 / 5, 0, Math.PI * 2);
    ctx.fill();
    ctx.closePath();
}

function drawLine(x1, y1, x2, y2) {
    ctx.beginPath();
    ctx.lineWidth = pen_size;
    ctx.lineCap = "round";
    ctx.lineJoin = "round";
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.stroke();
    ctx.closePath();
}

//undo redo
function save() {
    step++;
    if (step < memory.length) {
        memory.length = step;
    }
    memory.push(canvas.toDataURL());
}

function DrawUndo() {
    if (step > 0) {
        step--;
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        let pic = new Image();
        pic.src = memory[step];
        pic.onload = function() {
            ctx.drawImage(pic, 0, 0);
        }
    } else if (step == 0) {
        step = -1;
        ctx.clearRect(0, 0, canvas.width, canvas.height);
    }
}

function DrawRedo() {
    if (step <= memory.length - 1) {
        step++;
        let pic = new Image();
        pic.src = memory[step];
        pic.onload = function() {
            ctx.drawImage(pic, 0, 0);
        }
    }
}

function DrawUpload(e) {
    var reader = new FileReader();
    reader.onload = function(event) {
        var img = new Image();
        img.onload = function() {
            ctx.drawImage(img, 0, 0);
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);
}

function getCanvasCoordinates(e) {
    var x = event.clientX - canvas.getBoundingClientRect().left;
    var y = event.clientY - canvas.getBoundingClientRect().top;
    return { x: x, y: y };
}
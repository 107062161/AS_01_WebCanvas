# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use

首先你可以在左邊的工具列中選出你想用的工具（一開始是筆刷工具）

![](https://i.imgur.com/Pi95GRm.jpg)

然後在右邊的畫板（白色的部分）進行繪畫。
文字的部分需要先打，再左擊畫板放置!
文字大小同樣能使用`筆刷大小`來進行調整喔!

### Function description

首先我有用到的是Canvas
/...是一些基本的按鈕
```
<canvas id="drawing-board" ></canvas>
/...

```
然後在CSS裡面放按鈕/工具列的style
```
main .upload {
    display: block;
    width: 30px;
    height: 30px;
    background-image: url(../image/upload.png);
    background-color: yellow;
    color: #171717;
    font-size: 20px;
    font-weight: 900;
    border: none;
    outline: none;
    cursor: pointer
}
```

畫板我就用body來做
cursor選擇一開始就用筆刷的圖片
```
body {
    margin: 0;
    padding: 0;
    width: 100vw;
    height: 100vh;
    border: 1px solid black;
    background: #F3F3F3;
    font-family: 'Roboto', 'Helvetica', sans-serif;
    cursor: url('../image/pen.png'), auto;
}
```
最後到js的code

先init畫板
```
let canvas = document.getElementById("drawing-board");
let ctx = canvas.getContext('2d');
let screen_Width = document.documentElement.clientWidth;
let screen_Height = document.documentElement.clientHeight;

canvas.width = screen_Width;
canvas.height = screen_Height;
```

* 宣告
宣告variable，還有宣告工具的使用狀態。

```
//text
let text_btn = document.getElementById("text");
let isText = false;
var text_size;
//triangle
let tri_btn = document.getElementById("triangle");
let isTriangle = false;
//rectangle
let rec_btn = document.getElementById("rectangle");
let isRectangle = false;
//circle
let cir_btn = document.getElementById("circle");
let isCircle = false;
//line
let line_btn = document.getElementById("make-line");
let isLine = false;
//Drag
let isDragging = false;
var snapshot;
var dragStartLocation;
//download
let download = document.getElementById("save");
//upload
let imageLoader = document.getElementById('upload_img');
imageLoader.addEventListener("change", DrawUpload, false);
//redo undo
let redo = document.getElementById('redo');
let undo = document.getElementById('undo');
let memory = [];
let step = -1;
//color
let color = document.getElementById('favcolor');
//Pen size
let Size = document.getElementById('range1');
let pen_size = 5;
//Draw and bruch state 
let isDrawing = false;
let isBruching = true;
let bruch = document.getElementById("bruch");
//Mouse coordinate
let lastPoint = { x: undefined, y: undefined };
//Clear the screen
let clearScreen = document.getElementById("clear");
//Eraser
let isErasing = false;
let eraser = document.getElementById("eraser");
```
* 點擊
onclick我把它寫在js裡，不在html。
如果使用一個工具，就會把其他工具`= false`
再把`cursor = 那個工具的圖片`
例如是
```
cir_btn.onclick = function() {
    document.body.style.cursor = "url('./image/circle.png'), auto";
    isCircle = true;
    isLine = false;
    isBruching = false;
    isErasing = false;
    isTriangle = false;
    isRectangle = false;
    isText = false;
}
```

* 功能
1. 筆刷我會先把鼠標的x軸y軸存下來
再把`isBruching == true`狀態改變
```
    var x = event.clientX - canvas.getBoundingClientRect().left;
    var y = event.clientY - canvas.getBoundingClientRect().top;
//...
 (isBruching == true) {
        ctx.fillStyle = color.value;
        ctx.strokeStyle = color.value;
        drawCircle(x, y, pen_size);
}
```
然後在`mousedown` `mousemove`同時進行時，把新點擊的點跟舊點擊的點連成線，重覆直到`mouseup`完成。
```
if (isBruching) drawLine(lastPoint.x, lastPoint.y, newPoint.x, newPoint.y);
lastPoint = newPoint;
```
2. 顏色是用`type="color"`來取得value
```
<input type="color" id="favcolor" class="selectcolor" name="favcolor" value="#ff0000">
```
再用`fillStyle`跟`strokeStyle`來改變顏色
```
ctx.fillStyle = color.value;
ctx.strokeStyle = color.value;
```
3. 橡皮擦
把狀態改變，再用`clearRect`鼠標當前的位置`clear`掉
```
if (isErasing) {
     ctx.clearRect(x - 5, y - 5, pen_size, pen_size);
}
```
4. Undo
先把當前的圖存下來，按Undo時把存下來的圖release出來，再把步數-1。當步數==0的時候把他clear掉，再重設步數。
```
if (step > 0) {
    step--;
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    let pic = new Image();
    pic.src = memory[step];
    pic.onload = function() {
        ctx.drawImage(pic, 0, 0);
    }
} else if (step == 0) {
    step = -1;
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}
```
5. Redo
因為有先把圖save起來，所以把步數+1的圖release出來就可以了。
```
if (step <= memory.length - 1) {
    step++;
    let pic = new Image();
    pic.src = memory[step];
    pic.onload = function() {
        ctx.drawImage(pic, 0, 0);
    }
}
```
6. 上傳圖片
上傳圖片的type，在js裡面開一個`reader`把image讀下來，再把image由Canvas畫出來。
```
//html
 <input id="upload_img" style="display:none;" type="file">
 
//js
var reader = new FileReader();
reader.onload = function(event) {
    var img = new Image();
    img.onload = function() {
        ctx.drawImage(img, 0, 0);
    }
    img.src = event.target.result;
}
reader.readAsDataURL(e.target.files[0]);
```
7. 下載圖片
當按下下載按鈕，把現在的畫板資料載到路徑下。再來就是圖片的元素（名字...）。
```
download.onclick = function() {
    let imgUrl = canvas.toDataURL('image/png');
    let down = document.createElement('a');
    document.body.appendChild(down);
    down.href = imgUrl;
    down.download = "New"
    down.click();
}
```
8. 圖形
圖形利用拖動來實現
```
function dragStart(event) {
    isDragging = true;
    dragStartLocation = getCanvasCoordinates(event);
    takeSnapshot();
}

function drag(e) {
    var pos;
    if (isDragging === true) {
        restoreSnapshot();
        pos = getCanvasCoordinates(event);
        if (isCircle) draw2(pos);
        else if (isLine) draw1(pos);
        else if (isTriangle) draw3(pos);
        else draw4(pos);
    }
}

function dragStop() {
    restoreSnapshot();
    var pos = getCanvasCoordinates(event);
    if (isCircle) draw2(pos);
    else if (isLine) draw1(pos);
    else if (isTriangle) draw3(pos);
    else draw4(pos);
}
```
再來把圖片畫出來
例如是線:
```
function draw1(pos) { //Line
    ctx.beginPath();
    ctx.strokeStyle = color.value;
    ctx.lineWidth = pen_size;
    ctx.lineCap = 'round';
    ctx.moveTo(dragStartLocation.x, dragStartLocation.y);
    ctx.lineTo(pos.x, pos.y);
    ctx.stroke();
}
```
圓形:
```

function draw2(pos) { //Circle
    var radius = Math.sqrt(Math.pow((dragStartLocation.x - pos.x), 2) + Math.pow((dragStartLocation.y - pos.y), 2));
    ctx.beginPath();
    ctx.strokeStyle = color.value;
    ctx.lineWidth = pen_size;
    ctx.arc(dragStartLocation.x, dragStartLocation.y, radius, 0, 2 * Math.PI, false);
    ctx.stroke();
}
```
9. 文字
文字會先在左邊工具列輸入一個字串, 再把value傳給`fillText`畫出來
```
function drawText(e, x, y) {
    text_size = pen_size + 20;
    ctx.font = text_size + "pt" + " Arial";
    console.log(ctx.font);
    ctx.fillText(text.value, x, y);
}
```

10. 清除
簡單的功能，直接把畫板clear掉
```
clearScreen.onclick = function(e) {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
}
```

### Gitlab page link

https://gitlab.com/107062161/AS_01_WebCanvas/pages

### Others (Optional)

Thanks for reading

<style>
table th{
    width: 100%;
}
</style>